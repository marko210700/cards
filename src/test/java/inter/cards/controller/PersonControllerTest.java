package inter.cards.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import inter.cards.DTO.PersonDTO;
import inter.cards.Utils;
import inter.cards.entity.Person;
import inter.cards.entity.Status;
import inter.cards.repository.PersonRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PersonControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private PersonRepository personRepository;

    private static String cardsDir = "cards-test";

    ObjectMapper objectMapper = new ObjectMapper();

    private static final PersonDTO personDTO = Utils.createPersonDTO();

    @BeforeAll
    public static void setup() {
        //create directory for test files
        Path cardsDirPath = Paths.get("").resolve(cardsDir);
        boolean created = new File(cardsDirPath.toString()).mkdir();
        if (!created) {
            throw new RuntimeException("Could not create directory " + cardsDirPath);
        }
    }

    @AfterAll
    public static void teardown() throws Exception {
        //delete directory for test files
        Path cardsDirPath = Paths.get("").resolve(cardsDir);
        FileSystemUtils.deleteRecursively(cardsDirPath);
    }


    @Test
    @Transactional
    @Order(1)
    public void testSuccessfulCreatePerson_thenReturns201() throws Exception {
        mvc.perform(post("/api/v1/persons")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(personDTO)))
                .andExpect(status().isCreated())
                .andReturn();
        Person person = personRepository.findByOib(personDTO.getOib()).orElseThrow(EntityNotFoundException::new);
        assert (person.getStatus().equals(Status.IN_PROGRESS));
        personRepository.deleteByOib(personDTO.getOib());
    }

    @Test
    @Transactional
    @Order(2)
    public void testSuccessfulSearchPerson_thenReturns200() throws Exception {
        Person person = Utils.createPerson();
        personRepository.save(person);

        MvcResult result = mvc.perform(get("/api/v1/persons/search?oib=" + person.getOib()))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        PersonDTO personDTO1 = objectMapper.readValue(response, PersonDTO.class);
        assert (personDTO1.getOib().equals(personDTO.getOib()));
        assert (personDTO1.getStatus().equals(Status.IN_PROGRESS));
        //check that file was created
        checkFileCreated(person.getOib());

        personRepository.deleteByOib(personDTO.getOib());
    }


    @Test
    @Transactional
    @Order(3)
    public void testSearchPerson_thenReturns404() throws Exception {
        Person person = Utils.createPerson();
        personRepository.save(person);
        person.setOib("000000000");

        MvcResult result = mvc.perform(get("/api/v1/persons/search?oib=" + person.getOib() + "1"))
                .andExpect(status().isNotFound())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assert (response.equals("Entity not found!"));
        //check that file was not created
        List<Path> filePaths = getPaths(person.getOib());
        assert (filePaths.isEmpty());

        personRepository.deleteByOib(personDTO.getOib());
    }

    @Test
    @Transactional
    @Order(4)
    public void testDeletePerson_thenReturns200() throws Exception {
        Person person = Utils.createPerson();
        personRepository.save(person);

        mvc.perform(get("/api/v1/persons/search?oib=" + person.getOib()));//to create file so we can assure it is deactivated
        checkFileCreated(person.getOib());

        mvc.perform(delete("/api/v1/persons/" + personDTO.getOib()))
                .andExpect(status().isOk());


        //check that no active file is present
        List<Path> filePaths = getPaths(person.getOib());
        assert (filePaths.isEmpty());

        //assert file is deactivated
        List<Path> deactivatedFiles = getPaths("deactivated-" + person.getOib());
        assert (deactivatedFiles.size() == 1);

        //check that person is deactivated
        Person person1 = personRepository.findByOib(personDTO.getOib()).orElseThrow(EntityNotFoundException::new);
        assert (person1.getStatus().equals(Status.DEACTIVATED));
    }

    private static void checkFileCreated(String oib) throws IOException {
        List<Path> filePaths = getPaths(oib);
        assert (filePaths.size() == 1);
    }

    private static List<Path> getPaths(String prefix) throws IOException {
        Path cardsDirPath = Paths.get("").resolve(cardsDir);

        List<Path> filePaths = Files.list(cardsDirPath).filter(path -> path.getFileName().toString().startsWith(prefix)).toList();
        return filePaths;
    }
}
