package inter.cards;

import inter.cards.DTO.PersonDTO;
import inter.cards.entity.Person;
import inter.cards.entity.Status;

public class Utils {
    public static final String TEST_OIB = "12345678901";
    public static final String TEST_NAME = "Ivan";
    public static final String TEST_SURNAME = "Ivic";
    public static Person createPerson() {
        Person person = createPersonWithoutId();
        person.setId(1L);
        return person;
    }

    public static Person createPersonWithoutId() {
        Person person = new Person();
        person.setOib(TEST_OIB);
        person.setName(TEST_NAME);
        person.setSurname(TEST_SURNAME);
        person.setStatus(Status.IN_PROGRESS);
        return person;
    }

    public static PersonDTO createPersonDTO() {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setOib(TEST_OIB);
        personDTO.setName(TEST_NAME);
        personDTO.setSurname(TEST_SURNAME);
        personDTO.setStatus(Status.IN_PROGRESS);
        return personDTO;
    }
}
