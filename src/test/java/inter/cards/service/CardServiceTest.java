package inter.cards.service;

import inter.cards.entity.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Optional;
import java.util.stream.Stream;

import static inter.cards.Utils.createPerson;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CardServiceTest {

    @Mock
    Logger logger;

    @InjectMocks
    CardService cardService;

    //write test for deactivateCurrentFile
    @Test
    public void testCreateCardFile_successful () throws IOException {

        try (MockedStatic<Paths> pathsMocked = Mockito.mockStatic(Paths.class);
            MockedStatic<Files> filesMocked = Mockito.mockStatic(Files.class)) {
            //given
            Person person = createPerson();

            ReflectionTestUtils.setField(cardService, "CARDS_DIR", "cards-test");
            Path path = mock(Path.class);
            when(Paths.get("")).thenReturn(path);
            when(path.resolve("cards-test")).thenReturn(path);
            when(path.toAbsolutePath()).thenReturn(path);
            when(path.toString()).thenReturn("test-path");
            when(Paths.get(eq(path.toAbsolutePath().toString()), anyString())).thenReturn(path);
            when(Files.list(path)).thenReturn(Stream.empty());

            when(Files.write(path, person.getStringJoined().getBytes(), StandardOpenOption.CREATE)).thenReturn(path);

            //when
            cardService.createCardFile(person);

            //then
            verify(logger, never()).error(anyString(), Optional.ofNullable(any()));
        } catch (Exception e) {
            throw e;
        }
    }
}
