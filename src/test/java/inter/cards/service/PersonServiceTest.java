package inter.cards.service;

import inter.cards.DTO.PersonDTO;
import inter.cards.entity.Person;
import inter.cards.entity.Status;
import inter.cards.exception.DuplicateOibException;
import inter.cards.repository.PersonRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;

import java.io.IOException;
import java.util.Optional;

import static inter.cards.Utils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PersonServiceTest {
    @Mock
    private PersonRepository personRepository;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private CardService cardService;

    @InjectMocks
    private PersonService personService;


    @Test
    public void testCreatePerson_successful() {
        //given
        PersonDTO personDTO = createPersonDTO();
        Person person = createPersonWithoutId();
        Person personWithId = createPerson();

        when(modelMapper.map(personDTO, Person.class)).thenReturn(person);
        when(modelMapper.map(personWithId, PersonDTO.class)).thenReturn(personDTO);
        when(personRepository.save(person)).thenReturn(personWithId);

        //when
        PersonDTO personDTO1 = personService.createPerson(personDTO);

        //then
        assertNotNull(personDTO1);
        assertEquals(personDTO1.getOib(), personDTO.getOib());
    }

    @Test
    public void testCreatePerson_DuplicateOibException() {
        //given
        PersonDTO personDTO = createPersonDTO();
        Person person = createPerson();

        when(modelMapper.map(personDTO, Person.class)).thenReturn(person);
        when(personRepository.save(person)).thenThrow(new DataIntegrityViolationException("Duplicate entry"));

        //when
        assertThrows(DuplicateOibException.class, () -> personService.createPerson(personDTO));
    }

    @Test
    public void testSearchPerson_successful() throws IOException {
        //given
        Person person = createPerson();
        PersonDTO personDTO = createPersonDTO();

        when(personRepository.findByOib(anyString())).thenReturn(Optional.of(person));
        when(modelMapper.map(person, PersonDTO.class)).thenReturn(personDTO);
        doNothing().when(cardService).createCardFile(person);

        //when
        PersonDTO personDTO1 = personService.searchPerson(TEST_OIB);

        //then
        assertNotNull(personDTO1);
        assertEquals(personDTO1.getOib(), personDTO.getOib());
        verify(cardService, times(1)).createCardFile(person);
    }

    @Test
    public void testSearchPerson_EntityNotFoundException() {
        //given
        when(personRepository.findByOib(anyString())).thenReturn(Optional.empty());

        //when
        assertThrows(EntityNotFoundException.class, () -> personService.searchPerson(TEST_OIB));
    }

    @Test
    public void testSearchPerson_IOException() throws IOException {
        //given
        Person person = createPerson();

        when(personRepository.findByOib(anyString())).thenReturn(Optional.of(person));
        doThrow(new IOException()).when(cardService).createCardFile(person);

        //when
        assertThrows(IOException.class, () -> personService.searchPerson(TEST_OIB));
    }

    @Test
    public void testDeletePerson_successful() throws IOException {
        //given
        Person person = createPerson();
        person.setStatus(Status.DEACTIVATED);

        when(personRepository.findByOib(anyString())).thenReturn(Optional.of(person));
        doNothing().when(cardService).deactivateIfExist(person.getOib());
        when(personRepository.save(person)).thenReturn(person);

        //when
        personService.deletePerson(TEST_OIB);

        //then
        person = personRepository.findByOib(TEST_OIB).orElseThrow(EntityNotFoundException::new);
        assertEquals(person.getStatus(), Status.DEACTIVATED);
        verify(personRepository, times(1)).save(person);
        verify(cardService, times(1)).deactivateIfExist(person.getOib());
    }

    @Test
    public void testDeletePerson_EntityNotFoundException() {
        //given
        when(personRepository.findByOib(anyString())).thenReturn(Optional.empty());

        //when
        assertThrows(EntityNotFoundException.class, () -> personService.deletePerson(TEST_OIB));
    }

    @Test
    public void testDeletePerson_IOException() throws IOException {
        //given
        Person person = createPerson();
        Person person1 = createPerson();
        person1.setStatus(Status.DEACTIVATED);

        when(personRepository.findByOib(anyString())).thenReturn(Optional.of(person));
        doThrow(new IOException()).when(cardService).deactivateIfExist(person.getOib());

        //when
        assertThrows(IOException.class, () -> personService.deletePerson(TEST_OIB));
    }
}
