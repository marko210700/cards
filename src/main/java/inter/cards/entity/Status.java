package inter.cards.entity;

public enum Status {
    REGISTERED, IN_PROGRESS, DEACTIVATED
}
