package inter.cards.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.StringJoiner;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    @Column(unique = true)
    private String oib;
    private Status status;

    public String getStringJoined() {
        StringJoiner stringJoiner = new StringJoiner(",");
        stringJoiner.add(this.getName())
                .add(this.getSurname())
                .add(this.getOib())
                .add(this.getStatus().toString());
        return stringJoiner.toString();
    }
}
