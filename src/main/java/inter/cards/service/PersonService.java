package inter.cards.service;

import inter.cards.DTO.PersonDTO;
import inter.cards.entity.Person;
import inter.cards.entity.Status;
import inter.cards.exception.DuplicateOibException;
import inter.cards.repository.PersonRepository;
import jakarta.persistence.EntityNotFoundException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Service
public class PersonService {
    private static final Logger logger = LoggerFactory.getLogger(CardService.class);

    PersonRepository personRepository;
    ModelMapper modelMapper;
    CardService cardService;

    @Autowired
    public PersonService(PersonRepository personRepository, ModelMapper modelMapper, CardService cardService) {
        this.personRepository = personRepository;
        this.modelMapper = modelMapper;
        this.cardService = cardService;
    }


    public PersonDTO createPerson(PersonDTO personDTO) {
        logger.info("[createPerson-START]: Creating person with oib {}", personDTO.getOib());
        Person person = modelMapper.map(personDTO, Person.class);
        try {
            person = personRepository.save(person);
            personDTO = modelMapper.map(person, PersonDTO.class);
            logger.info("[createPerson-END]: Person with oib {} created", personDTO.getOib());
            return personDTO;
        } catch (DataIntegrityViolationException e) {
            logger.error("[createPerson]: Error creating person with oib {}", personDTO.getOib(), e);
            throw new DuplicateOibException("Person with oib " + personDTO.getOib() + " already exists");
        }
    }

    public PersonDTO searchPerson(String oib) throws IOException {
        logger.info("[searchPerson-START]: Searching person with oib {}", oib);
        Optional<Person> personOptional = personRepository.findByOib(oib);
        if (personOptional.isPresent()) {
            Person person = personOptional.get();
            cardService.createCardFile(person);
            person.setStatus(Status.IN_PROGRESS);
            personRepository.save(person);
            PersonDTO personDTO = modelMapper.map(person, PersonDTO.class);
            logger.info("[searchPerson-END]: Person with oib {} found", oib);
            return personDTO;
        } else {
            logger.error("[searchPerson]: Person with oib {} not found", oib);
            throw new EntityNotFoundException("Person with oib " + oib + " not found");
        }
    }

    public void deletePerson(String oib) throws IOException {
        //Since it was mentioned that person is permanently saved to database and that
        //it would be good to "play" with Status field I do not delete person from database
        //but rather set Status to DEACTIVATED
        logger.info("[deletePerson-START]: Deleting person with oib {}", oib);
        Optional<Person> personOptional = personRepository.findByOib(oib);
        if (personOptional.isPresent()) {
            Person person = personOptional.get();
            cardService.deactivateIfExist(person.getOib());
            person.setStatus(Status.DEACTIVATED);
            personRepository.save(person);
            logger.info("[deletePerson-END]: Person with oib {} deleted", oib);
        } else {
            logger.error("[deletePerson]: Person with oib {} not found", oib);
            throw new EntityNotFoundException("Person with oib " + oib + " not found");
        }
    }
}
