package inter.cards.service;

import inter.cards.entity.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Service
public class CardService {

    private static final Logger logger = LoggerFactory.getLogger(CardService.class);

    @Value("${cards.dir}")
    public String CARDS_DIR;

    static void deactivateCurrentFile(List<Path> paths, String oib) throws IOException {
        logger.info("[deactivateCurrentFile]: Deactivating card file for person with oib {}", oib);
            for (Path path : paths) {//if for some reason there are more than one file with same oib
                try {
                    Files.move(path, path.resolveSibling("deactivated-" + path.getFileName().toString()));
                } catch (IOException e) {
                    logger.error("[deactivateCurrentFile]: Error deleting file {}", path, e);
                    throw e;
                }
            }
    }

    public void createCardFile(Person person) throws IOException {
        logger.info("[createCardFile-START]: Creating card file for person with oib {}", person.getOib());
        //create txt file that will have one row populated with person data with name oib-timestamp.txt and delimiter is |
        String stringJoinedPerson = person.getStringJoined();

        try {
            //check if file wit oib-timestamp.txt exists and deactivate such if so
            Path cardsDirPath = Paths.get("").resolve(CARDS_DIR);
            List<Path> deactivationList = getActiveFiles(person.getOib(), cardsDirPath);

            if (!deactivationList.isEmpty()) {
                deactivateCurrentFile(deactivationList, person.getOib());
            }

            Date currentDate = new Date();
            String formattedDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(currentDate);

            Path filePath = Paths.get(cardsDirPath.toAbsolutePath().toString(), person.getOib() + "_" + formattedDate + ".txt");
            logger.info("[createCardFile]: File path {}", filePath);

            Files.write (filePath, stringJoinedPerson.getBytes(), StandardOpenOption.CREATE);
            logger.info("[createCardFile-END]: Card file created for person with oib {}", person.getOib());
        } catch (IOException e) {
            //TOOD: handle exception
            logger.error("[createCardFile]: Error creating card file for person with oib {}", person.getOib(), e);
            throw e;
        }
    }

    private static List<Path> getActiveFiles(String oib, Path cardsDirPath) throws IOException {
        return Files.list(cardsDirPath).
                filter(path -> path.getFileName().toString().startsWith(oib)).toList();
    }

    public void deactivateIfExist(String oib) throws IOException {
        logger.info("[deactivateIfExist-START]: Deactivating card file for person with oib {}", oib);
        try {
            Path cardsDirPath = Paths.get("").resolve(CARDS_DIR);

            List<Path> deactivationList = getActiveFiles(oib, cardsDirPath);

            if (!deactivationList.isEmpty()) {
                deactivateCurrentFile(deactivationList, oib);
            }
            logger.info("[deactivateIfExist-END]: Card file deactivated for person with oib {}", oib);
        } catch (IOException e) {
            logger.error("[deactivateIfExist]: Error deactivating card file for person with oib {}", oib, e);
            throw new IOException("Error deactivating card file for person with oib " + oib, e);
        }

    }
}
