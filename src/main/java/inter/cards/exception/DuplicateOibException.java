package inter.cards.exception;

public class DuplicateOibException extends RuntimeException {

    public DuplicateOibException(String message) {
        super(message);
    }

    public DuplicateOibException(String message, Throwable cause) {
        super(message, cause);
    }
}
