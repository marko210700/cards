package inter.cards.controller;

import inter.cards.DTO.PersonDTO;
import inter.cards.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController()
@RequestMapping("/api/v1/persons")
public class PersonController {

    PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }
    //createPerson
    @PostMapping
    public ResponseEntity<PersonDTO> createPerson(@RequestBody PersonDTO personDTO) {
        PersonDTO personDTO1 = personService.createPerson(personDTO);
        return ResponseEntity.status(201).body(personDTO1);
    }

    @GetMapping("/search")
    public ResponseEntity<PersonDTO> searchPerson(@RequestParam String oib) throws IOException {
        PersonDTO personDTO = personService.searchPerson(oib);
        return ResponseEntity.ok(personDTO);
    }

    @DeleteMapping("/{oib}")
    public ResponseEntity<String> deletePerson(@PathVariable String oib) throws IOException {
        personService.deletePerson(oib);
        return ResponseEntity.ok("Person deleted");
    }
}
