package inter.cards.DTO;

import inter.cards.entity.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PersonDTO {
    private String name;
    private String surname;
    private String oib;
    private Status status;
}
